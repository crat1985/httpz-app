const std = @import("std");
const httpz = @import("httpz");

pub const HttpServer = struct {
    allocator: std.mem.Allocator,
    server: httpz.ServerCtx(void, void),

    pub fn init(allocator: std.mem.Allocator, port: u16) !HttpServer {
        var server = try httpz.Server().init(allocator, .{
            .port = port,
        });

        var router = server.router();
        router.get("/", ok_route);

        const serv = HttpServer{ .allocator = allocator, .server = server };

        return serv;
    }

    pub fn listen(self: *HttpServer) !void {
        try self.server.listen();
    }

    pub fn deinit(self: *HttpServer) void {
        self.server.deinit();
    }
};

fn ok_route(_: *httpz.Request, res: *httpz.Response) !void {
    try res.json(.{ .slt = "truc", .status = "Ok", .not_displayed = @as(?u8, null) }, .{ .whitespace = .indent_4, .emit_null_optional_fields = false });
}
