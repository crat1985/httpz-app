const std = @import("std");

/// Caller must free the returned memory
pub fn get_env_variable(allocator: std.mem.Allocator, name: []const u8) ![]u8 {
    return std.process.getEnvVarOwned(allocator, name);
}
