const std = @import("std");
const smtp = @import("smtp");
const env = @import("env.zig");

test SmtpClient {
    const allocator = std.testing.allocator;
    _ = try SmtpClient.init(allocator);
    defer smtp_client.deinit();

    const dest = try env.get_env_variable(allocator, "TEST_SMTP_DEST");
    defer allocator.free(dest);

    try smtp_client.send_html_mail(&.{dest}, "Title", "<h1>Content</h1>\r\n<b><i>Hi</i></b>");
}

var smtp_client: SmtpClient = undefined;

/// The SMTP Client **must** have been initiated
pub fn getSmtpClient() SmtpClient {
    return smtp_client;
}

pub const SmtpClient = struct {
    allocator: std.mem.Allocator,
    smtpUsername: []const u8,
    smtpClient: smtp.Client,

    pub fn init(allocator: std.mem.Allocator) !SmtpClient {
        const smtpHost = try env.get_env_variable(allocator, "SMTP_HOST");
        defer allocator.free(smtpHost);

        const smtpPortStr =
            try env.get_env_variable(allocator, "SMTP_PORT");

        const smtpPort = try std.fmt.parseInt(u16, smtpPortStr, 10);
        allocator.free(smtpPortStr);

        const smtpUsername =
            try env.get_env_variable(allocator, "SMTP_USERNAME");

        const smtpPassword =
            try env.get_env_variable(allocator, "SMTP_PASSWORD");
        defer allocator.free(smtpPassword);

        const config = smtp.Config{ .host = smtpHost, .port = smtpPort, .allocator = allocator, .username = smtpUsername, .password = smtpPassword };

        var client = try smtp.connect(config);

        try client.hello();
        try client.auth();

        smtp_client = SmtpClient{ .allocator = allocator, .smtpUsername = smtpUsername, .smtpClient = client };

        return smtp_client;
    }

    /// The SMTP Client **must** have been initiated
    /// Lines have a maximum length of 1000 (including the trailing \r\n)
    /// Any line that begins with a '.' must be escaped with a '.' (in regex talk: s/^\./../)
    pub fn send_html_mail(self: *SmtpClient, to: []const []const u8, subject: []const u8, body: []const u8) !void {
        if (to.len == 0) {
            return error.MissingTo;
        }

        const tos = try std.mem.join(self.allocator, ", ", to);
        defer self.allocator.free(tos);

        const full_data = try std.fmt.allocPrint(self.allocator, "From: FlashDebat <{s}>\r\nTo: {s}\r\nSubject: {s}\r\nContent-Type: text/html;\r\n\r\n{s}\r\n.\r\n", .{ self.smtpUsername, tos, subject, body });
        defer self.allocator.free(full_data);

        try self.smtpClient.from(self.smtpUsername);
        try self.smtpClient.to(to);
        try self.smtpClient.data(full_data);
    }

    pub fn deinit(self: *SmtpClient) void {
        self.allocator.free(self.smtpUsername);
        self.smtpClient.deinit();
    }
};
