const std = @import("std");
const httpz = @import("httpz");
const websocket = httpz.websocket;

pub const WebSocketServer = struct {
    allocator: std.mem.Allocator,
    context: @TypeOf(.{}),
    port: u16,

    pub fn init(allocator: std.mem.Allocator, port: u16) !WebSocketServer {
        return .{ .allocator = allocator, .context = .{}, .port = port };
    }

    pub fn listen(self: *WebSocketServer) !void {
        try websocket.listen(Handler, self.allocator, self.context, .{
            .port = self.port,
        });
    }

    const Handler = struct {
        conn: *websocket.Conn,
        context: @TypeOf(.{}),

        pub fn init(_: websocket.Handshake, conn: *websocket.Conn, context: @TypeOf(.{})) !Handler {
            return Handler{ .conn = conn, .context = context };
        }

        //Optional
        pub fn afterInit(_: *Handler) !void {}

        pub fn handle(self: *Handler, message: websocket.Message) !void {
            const data = message.data;
            try self.conn.write(data);
        }

        pub fn close(_: *Handler) void {}
    };
};
