const std = @import("std");
const Db = @import("db.zig").Db;
const validate = @import("validate");
const SmtpClient = @import("smtp_client.zig").SmtpClient;
const WebSocketServer = @import("websocket_server.zig").WebSocketServer;
const HttpServer = @import("http_server.zig").HttpServer;

test {
    _ = @import("db.zig");
    _ = @import("smtp_client.zig");
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    var smtp = try SmtpClient.init(allocator);
    defer smtp.deinit();

    var db = try Db.init(allocator);
    defer db.deinit();

    var threads: [2]std.Thread = undefined;

    var http_server = try HttpServer.init(allocator, 8080);
    defer http_server.deinit();

    const http_server_thread = try std.Thread.spawn(.{}, HttpServer.listen, .{&http_server});
    threads[0] = http_server_thread;

    var ws_server = try WebSocketServer.init(allocator, 8888);

    const ws_server_thread = try std.Thread.spawn(.{}, WebSocketServer.listen, .{&ws_server});
    threads[1] = ws_server_thread;

    std.debug.print("HTTP server listening on http://localhost:8080 and WebSocket server listening on ws://localhost:8888\n", .{});

    for (threads) |thread| thread.join();
}
