const std = @import("std");
const pg = @import("pg");
const env = @import("env.zig");
const build = @import("build");

test Db {
    var pool_local = try Db.init(std.testing.allocator);
    defer pool_local.deinit();
}

var db: Db = undefined;

/// The pool **must** have been initialized beforehand
pub fn getDb() *pg.Pool {
    return db;
}

pub const Db = struct {
    allocator: std.mem.Allocator,
    pool: *pg.Pool,

    pub fn init(allocator: std.mem.Allocator) !Db {
        const pool = try pg.Pool.init(allocator, .{ .auth = .{
            .username = try env.get_env_variable(allocator, "PG_USERNAME"),
            .password = try env.get_env_variable(allocator, "PG_PASSWORD"),
            .database = try env.get_env_variable(allocator, "PG_DB"),
        } });

        db = Db{
            .allocator = allocator,
            .pool = pool,
        };

        try db.migrate();

        return db;
    }

    fn migrate(self: *Db) !void {
        const migrations =
            @import("config").migrations;

        _ = try self.pool.exec(
            \\CREATE TABLE IF NOT EXISTS _migrations (
            \\id INT PRIMARY KEY,
            \\hash VARCHAR NOT NULL
            \\);
        , .{});

        for (migrations, 1..) |migration, id| {
            try self.executeOneMigration(migration, id);
        }
    }

    fn executeOneMigration(self: *Db, migration: build.Migration, id: usize) !void {
        const conn = try self.pool.acquire();
        defer conn.release();

        try conn.begin();
        errdefer conn.rollback() catch |err| std.debug.panic("{any}", .{err});

        const query_result = try conn.query("SELECT hash FROM _migrations WHERE id = $1", .{id});
        defer query_result.deinit();

        if (query_result.number_of_columns == 0) {
            _ = try conn.exec(migration, .{});
            try conn.exec("INSERT INTO _migrations (id, hash) VALUES ($1, $2);", .{ id, migration.hash });
        } else {
            const only_row = (try query_result.next()).?;
            const hash = only_row.get([]const u8, 0);
            if (hash != migration.hash) {
                return error.HashOfMigrationChanged;
            }
        }

        try conn.commit();
    }

    pub fn deinit(self: *Db) void {
        const auth_field = self.pool._opts.auth;
        const allocator = self.allocator;

        allocator.free(auth_field.username);

        if (auth_field.password) |password| allocator.free(password);

        if (auth_field.database) |database| allocator.free(database);

        self.pool.deinit();
    }
};
