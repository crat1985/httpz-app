const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "httpz_app",
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });

    const httpz = b.dependency("httpz", .{
        .target = target,
        .optimize = optimize,
    });

    exe.root_module.addImport("httpz", httpz.module("httpz"));

    const pg = b.dependency("pg", .{ .target = target, .optimize = optimize });

    exe.root_module.addImport("pg", pg.module("pg"));

    const smtp_client = b.dependency("smtp_client", .{
        .target = target,
        .optimize = optimize,
    });

    exe.root_module.addImport("smtp", smtp_client.module("smtp_client"));

    const validate = b.dependency("validate", .{ .target = target, .optimize = optimize });

    exe.root_module.addImport("validate", validate.module("validate"));

    const logz = b.dependency("logz", .{ .target = target, .optimize = optimize });

    exe.root_module.addImport("logz", logz.module("logz"));

    // Add migrations
    const options = setup_migrations(b) catch |err| {
        std.debug.print("{any}\n", .{err});
        @panic("");
    };

    exe.root_module.addOptions("config", options);

    // This declares intent for the executable to be installed into the
    // standard location when the user invokes the "install" step (the default
    // step when running `zig build`).
    b.installArtifact(exe);

    // This *creates* a Run step in the build graph, to be executed when another
    // step is evaluated that depends on it. The next line below will establish
    // such a dependency.
    const run_cmd = b.addRunArtifact(exe);

    // By making the run step depend on the install step, it will be run from the
    // installation directory rather than directly from within the cache directory.
    // This is not necessary, however, if the application depends on other installed
    // files, this ensures they will be present and in the expected location.
    run_cmd.step.dependOn(b.getInstallStep());

    // This allows the user to pass arguments to the application in the build
    // command itself, like this: `zig build run -- arg1 arg2 etc`
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    // This creates a build step. It will be visible in the `zig build --help` menu,
    // and can be selected like this: `zig build run`
    // This will evaluate the `run` step rather than the default, which is "install".
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const exe_unit_tests = b.addTest(.{
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });

    exe_unit_tests.root_module.addImport("httpz", httpz.module("httpz"));
    exe_unit_tests.root_module.addImport("pg", pg.module("pg"));
    exe_unit_tests.root_module.addImport("smtp", smtp_client.module("smtp_client"));
    exe_unit_tests.root_module.addImport("validate", validate.module("validate"));
    exe_unit_tests.root_module.addImport("logz", logz.module("logz"));
    exe_unit_tests.root_module.addOptions("config", options);

    const run_exe_unit_tests = b.addRunArtifact(exe_unit_tests);

    // Similar to creating the run step earlier, this exposes a `test` step to
    // the `zig build --help` menu, providing a way for the user to request
    // running the unit tests.
    const test_step = b.step("test", "Run unit tests");
    //test_step.dependOn(&run_lib_unit_tests.step);
    test_step.dependOn(&run_exe_unit_tests.step);
}

pub const Migration = struct { file_content: []const u8, hash: [64]u8 };

fn setup_migrations(b: *std.Build) !*std.Build.Step.Options {
    var migrations_with_index = std.AutoHashMap(usize, Migration).init(b.allocator);
    defer migrations_with_index.deinit();

    var dir = try std.fs.cwd().openDir("migrations", .{ .iterate = true });
    defer dir.close();
    var iter = dir.iterate();

    while (try iter.next()) |file| {
        if (file.kind != .file) continue;

        //Get relative path
        const absolute_file_path = try dir.realpathAlloc(b.allocator, file.name);
        defer b.allocator.free(absolute_file_path);

        //Get file number
        const fileNumberStr = file.name[0..4];
        const fileNumber = try std.fmt.parseInt(u32, fileNumberStr, 10);

        //Read file
        const openedFile = try std.fs.openFileAbsolute(absolute_file_path, .{});
        defer openedFile.close();

        const fileContent = try openedFile.readToEndAlloc(b.allocator, 1_000_000_000);

        //var hash: []u8 = try b.allocator.alloc(u8, 64);
        var hash: [64]u8 = undefined;

        std.crypto.hash.sha3.Sha3_512.hash(fileContent, &hash, .{});

        //TODO replace by getOrPut to detect if there was data
        try migrations_with_index.put(fileNumber, .{ .file_content = fileContent, .hash = hash });
    }

    var migrations = try std.ArrayList(Migration).initCapacity(b.allocator, migrations_with_index.count());
    defer migrations.deinit();

    for (0..migrations_with_index.count()) |i| {
        const migration =
            migrations_with_index.get(i + 1) orelse std.debug.panic("Missing migration {d}", .{i + 1});
        migrations.insertAssumeCapacity(i, migration);
    }

    const migrations_slice = try migrations.toOwnedSlice();
    defer b.allocator.free(migrations_slice);

    const options = b.addOptions();
    options.addOption([]const Migration, "migrations", migrations_slice);

    return options;
}

fn migrations_files_sort(_: void, a: Migration, b: Migration) bool {
    return std.sort.asc(u32)({}, a.index, b.index);
}
