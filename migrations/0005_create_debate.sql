CREATE TABLE IF NOT EXISTS debate (
  id BIGINT PRIMARY KEY,
  author_id BIGINT NOT NULL REFERENCES account(id) ON DELETE CASCADE,
  title VARCHAR(50),
  content VARCHAR(1000),
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  deleted BIGINT REFERENCES deleted(id) DEFAULT NULL

  CHECK (
    (
      title IS NOT NULL
      AND content IS NOT NULL
    ) OR (
      deleted IS NOT NULL
    )
  )
);
