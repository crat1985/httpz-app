CREATE TABLE IF NOT EXISTS email_verification (
    user_id BIGINT NOT NULL UNIQUE,
    email_verification_token VARCHAR PRIMARY KEY,
    expired_at TIMESTAMPTZ NOT NULL DEFAULT NOW() + INTERVAL '1 hour',
    username VARCHAR(15) NOT NULL UNIQUE,
    display_name VARCHAR(32) NOT NULL,
    email VARCHAR(1000) NOT NULL,
    password VARCHAR(128) NOT NULL,
    birthdate TIMESTAMPTZ NOT NULL,
    theme THEME NOT NULL DEFAULT 'dark',
    is_male BOOLEAN,
    lang VARCHAR(5) NOT NULL DEFAULT 'en_US'
);
