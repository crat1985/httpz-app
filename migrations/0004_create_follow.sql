DROP TYPE IF EXISTS follow_notification_level;
CREATE TYPE follow_notification_level AS ENUM ('new_posts_and_responses', 'new_posts', 'none');

CREATE TABLE IF NOT EXISTS follow (
  follower_id BIGINT REFERENCES account(id),
  followed_id BIGINT REFERENCES account(id),
  notification_level FOLLOW_NOTIFICATION_LEVEL NOT NULL DEFAULT 'none',
  PRIMARY KEY (follower_id, followed_id)
);
