DROP TYPE IF EXISTS theme;
CREATE TYPE theme AS ENUM ('dark', 'light');

CREATE TABLE IF NOT EXISTS account (
    id  BIGINT PRIMARY KEY,
    username VARCHAR(15) UNIQUE,
    display_name VARCHAR(32),
    email VARCHAR(1000) UNIQUE,
    password VARCHAR(128),
    birthdate TIMESTAMPTZ,
    theme THEME NOT NULL DEFAULT 'dark',
    biography VARCHAR(300) DEFAULT '',
    is_male BOOLEAN,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    banned_at TIMESTAMPTZ DEFAULT NULL,
    permission INT NOT NULL DEFAULT 0,
    deleted BIGINT REFERENCES deleted(id) DEFAULT NULL,
    lang VARCHAR(5) DEFAULT 'en_US'

    CHECK (
        (
            username IS NOT NULL
                AND display_name IS NOT NULL
                AND email IS NOT NULL
                AND password IS NOT NULL
                AND birthdate IS NOT NULL
                AND biography IS NOT NULL
                AND lang IS NOT NULL
        ) OR (
            deleted IS NOT NULL
        )
	)
);
