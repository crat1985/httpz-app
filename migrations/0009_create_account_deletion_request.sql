CREATE TABLE IF NOT EXISTS account_deletion_request (
    account_id BIGINT REFERENCES account(id) NOT NULL,
    token TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
